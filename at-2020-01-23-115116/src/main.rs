// snippet of code @ 2020-01-23 11:51:16

// === Rust Playground ===
// This snippet is in: ~/.emacs.d/rust-playground/at-2020-01-23-115116/

// Execute the snippet: C-c C-c
// Delete the snippet completely: C-c k
// Toggle between main.rs and Cargo.toml: C-c b

use pin_project::{pin_project, project};
use std::pin::Pin;

#[pin_project]
enum Foo<A, B, C> {
    Tuple(#[pin] A, B),
    Struct { field: C },
    Unit,
}

impl<A, B, C> Foo<A, B, C> {
    #[project] // Nightly does not need a dummy attribute to the function.
    fn baz(self: Pin<&mut Self>) {
        #[project]
        match self.project() {
            Foo::Tuple(x, y) => {
                let _: Pin<&mut A> = x;
                let _: &mut B = y;
            }
            Foo::Struct { field } => {
                let _: &mut C = field;
            }
            Foo::Unit => {}
        }
    }
}

fn main() {}
