// snippet of code @ 2019-10-14 16:47:24

// === Rust Playground ===
// This snippet is in: ~/.emacs.d/rust-playground/at-2019-10-14-164719/

// Execute the snippet: C-c C-c
// Delete the snippet completely: C-c k
// Toggle between main.rs and Cargo.toml: C-c b

extern crate combine;

use combine::{parser, Stream, Parser, ParseError, between, token, char::string, attempt};

#[inline]
fn para<Input, P, O>(p: P) -> impl Parser<Input, Output = P::Output>
where
    P: Parser<Input, Output = O>,
    Input: Stream<Token = char>,
    Input::Error: ParseError<Input::Token, Input::Range, Input::Position> {

    para_(p)
}

parser! {
    fn para_[Input, P, O](p: P)(Input) -> P::Output
    where [
        P: Parser<Input, Output = O>,
        Input: Stream<Token = char>,
        Input::Error: ParseError<Input::Token, Input::Range, Input::Position>,
    ] {
        attempt(token('(')).with(para(p)).skip(attempt(token(')')))
    }
}

fn main() {
    println!("result: {:?}", para(string("hello")).parse("hello"));
}
