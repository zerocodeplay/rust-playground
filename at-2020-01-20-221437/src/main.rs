// snippet of code @ 2020-01-20 22:14:37

// === Rust Playground ===
// This snippet is in: ~/.emacs.d/rust-playground/at-2020-01-20-221437/

// Execute the snippet: C-c C-c
// Delete the snippet completely: C-c k
// Toggle between main.rs and Cargo.toml: C-c b
use metered::{metered, HitCount, Throughput};

fn main() {}
