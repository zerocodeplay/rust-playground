// snippet of code @ 2020-01-08 15:44:46

// === Rust Playground ===
// This snippet is in: ~/.emacs.d/rust-playground/at-2020-01-08-154446/

// Execute the snippet: C-c C-c
// Delete the snippet completely: C-c k
// Toggle between main.rs and Cargo.toml: C-c b

use combine::{
    error::{ParseError, UnexpectedParse},
    parser::char::{char, digit, letter, space},
    stream::{
        easy::{self, Error, Errors},
        position::{self, SourcePosition},
        RangeStream,
    },
    Parser,
};

use std::{
    borrow::Cow,
    error::Error as StdError,
    fmt,
    io::{BufRead, BufReader, Cursor, Read},
    iter::Iterator,
    sync::atomic::{AtomicUsize, Ordering},
};

const WHITESPACES: &'static [char] = &['\t', '\n', '\x0C', '\r', ' '];

// trait TextStorage {
//     type Id : Sized + fmt::Debug;
//     type Item : Sized;
//     type Error : StdError;
//     ///
//     ///
//     ///
//     fn insert(&mut self, w: &'_ str) -> Result<Self::Id, Self::Error>;
//     ///
//     /// For non-in memory, when fetching item from storage
//     /// it might able return an error.
//     ///
//     fn fetch(&self, id: Self::Id) -> Result<Option<Self::Item>, Self::Error>;
// }

// trait ParseTree {
//     type Node : Sized;
//     type NodeIter : Iterator<Item = Self::Node>;

//     fn children(&self) -> Option<Self::NodeIter>;
// }

trait SyntaxTree {
    type Node: Sized;
    type NodeIter: Iterator<Item = Self::Node>;

    fn children(&self) -> Option<Self::NodeIter>;
}

// struct TextInMemStorage<'a> {
//     counter: AtomicUsize,
//     inner: Cow<&'a, [str]>,
// }

// impl <'a> TextInMemStorage<'a> {
//     #[inline]
//     pub fn fetch<'_>(&self, id: Id) -> Option<&'_ str> {
//         self.inner.get(id)
//     }

//     #[inline]
//     pub fn push(&mut self, w: &'_ str) -> Id {
//         self.inner.push(w);
//         self.counter.fetch_add(1, Ordering::SeqCst)
//     }
// }

/// Why not include number repr in toplevel ?
/// - each template may have it's own number repr outside IEEE754,
///   ex: [yaml](https://yaml.org/type/int.html).
///
///
#[derive(Debug)]
enum Token<'a> {
    // any readable characters outside symbols & double, integer
    Word(&'a str),
    // symbol repr, out of whitespace and readable utf8 characters
    Symbol(char),
    // whitespace char repr
    Whitespace(char, usize),
}

///
/// It will returns either `Token::Word` or `Token::Text`
///
// #[inline]
// fn tokens<'a, I>() -> impl Parser<Input = I, Output = Token>
// where
//     I : RangeStream<Token = char, Range = &'a str>,
//     I::Error : ParseError<I::Token, I::Range, I::Position> {
//
//     // check in order
//     // - it's a whitespace or not, if its whitespace take until next is not a whitespace
//     //   also aggregates the characters in whitespaces
//     // - else, check whether it's an alphanumeric then aggregates until found character
//     //   which not an alphanumeric
//     // - default: then it's symbol
//
//     many1().then(move |mut s: String|)
// }

fn main() {
    // the toplevel parser are only augments & aggregates symbols
    // whitespaces
    // word
    //

    let data = "hello world";
}
