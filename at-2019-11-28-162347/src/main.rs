// snippet of code @ 2019-11-28 16:23:47

// === Rust Playground ===
// This snippet is in: ~/.emacs.d/rust-playground/at-2019-11-28-162347/

// Execute the snippet: C-c C-c
// Delete the snippet completely: C-c k
// Toggle between main.rs and Cargo.toml: C-c b

use serde::de;

pub struct SeqAccess {}

impl<'de> de::SeqAccess<'de> for SeqAccess {
    type Error = ();
}

fn main() {
    let raw = r#"SAMPLE_DATA=test,123,t423123,"hello world""#;

    println!("Results:")
}
