// snippet of code @ 2020-01-07 21:20:07

// === Rust Playground ===
// This snippet is in: ~/.emacs.d/rust-playground/at-2020-01-07-212007/

// Execute the snippet: C-c C-c
// Delete the snippet completely: C-c k
// Toggle between main.rs and Cargo.toml: C-c b

use std::{convert, env};

pub trait Shell {
    type Item;
    type Error;

    fn name<'a>() -> &'a str;
    fn complete(item: &Item) -> Result<_, Error>;
    fn paths<'a>() -> Iterator<Item = &'a (str, str)>;
}

pub struct BashShell;
pub struct ZshShell;
pub struct FishShell;

pub struct ShellManager<S>
where
    S: Shell + Sized,
{
    inner: Vec<S>,
}

fn main() {
    println!("Results:")
}
