// snippet of code @ 2019-11-21 14:38:06

// === Rust Playground ===
// This snippet is in: ~/.emacs.d/rust-playground/at-2019-11-21-143806/

// Execute the snippet: C-c C-c
// Delete the snippet completely: C-c k
// Toggle between main.rs and Cargo.toml: C-c b
extern crate serde;
extern crate serde_json;

use std::sync::atomic::{AtomicUsize, Ordering};

static INDEX: AtomicUsize = AtomicUsize::new(0);

use serde::{Deserialize, Serialize};
use std::default::Default;

pub struct Sample2 {
    id: usize,
    value: usize,
}

impl Default for Sample2 {
    #[inline]
    fn default() -> Self {
        Self {
            id: 0usize,
            value: 0usize,
        }
    }
}

#[derive(Serialize, Deserialize, Debug)]
pub struct Sample<'a> {
    id: usize,
    name: &'a str,
    value: Sample2,
}

impl<'a> Sample<'a> {
    #[inline]
    pub fn new<'b>(name: &'b str) -> Self
    where
        'b: 'a,
    {
        Self {
            id: INDEX.fetch_add(1, Ordering::Relaxed),
            name: name,
            value: Sample2::default(),
        }
    }
}

fn main() -> Result<(), serde_json::Error> {
    let sample = Sample::new("hello world");
    let serialize = serde_json::to_string(&sample)?;

    println!("{}", serialize);

    let result: Sample = serde_json::from_str(&serialize)?;

    println!("Results: {:?}", result);

    Ok(())
}
