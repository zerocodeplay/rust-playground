// snippet of code @ 2019-11-05 15:02:31

// === Rust Playground ===
// This snippet is in: ~/.emacs.d/rust-playground/at-2019-11-05-150227/

// Execute the snippet: C-c C-c
// Delete the snippet completely: C-c k
// Toggle between main.rs and Cargo.toml: C-c b

use serde::de::{
    self, DeserializeSeed, EnumAccess, IntoDeserializer, MapAccess, SeqAccess, VariantAccess,
    Visitor,
};
use serde::Deserialize;

fn main() {
    println!("Results:")
}
