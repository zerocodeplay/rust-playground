// snippet of code @ 2019-12-09 20:56:37

// === Rust Playground ===
// This snippet is in: ~/.emacs.d/rust-playground/at-2019-12-09-205637/

// Execute the snippet: C-c C-c
// Delete the snippet completely: C-c k
// Toggle between main.rs and Cargo.toml: C-c b

pub enum ControlData {
    Raw = 0,
    Signed = 1,
    Unsigned = 2,
    Boolean = 3,
    Enum = 4,
    BitMask = 5,
}

pub struct FormatDesc {}

// https://github.com/codablock/linux-btrfs/blob/master/drivers/media/video/uvc/uvcvideo.h#L597-L699

fn main() {
    println!("Results:")
}
