// snippet of code @ 2020-01-29 20:04:54

// === Rust Playground ===
// This snippet is in: ~/.emacs.d/rust-playground/at-2020-01-29-200454/

// Execute the snippet: C-c C-c
// Delete the snippet completely: C-c k
// Toggle between main.rs and Cargo.toml: C-c b

use lock_api::{RwLockUpgradableReadGuard, RwLockWriteGuard};
use parking_lot::RwLock;

fn main() {
    let lock = RwLock::new(10);

    let mut rlock = lock.upgradable_read();

    println!("old value: {}", *rlock);

    if *rlock == 10 {
        let mut wlock = RwLockUpgradableReadGuard::upgrade(rlock);
        *wlock = 11;
    }

    println!("new value: {}", *lock.read());
}
