// snippet of code @ 2020-01-07 16:59:52

// === Rust Playground ===
// This snippet is in: ~/.emacs.d/rust-playground/at-2020-01-07-165952/

// Execute the snippet: C-c C-c
// Delete the snippet completely: C-c k
// Toggle between main.rs and Cargo.toml: C-c b

// tutorial for references & mut references

use std::io;

pub struct S<W>
where
    W: io::Write + Sized,
{
    inner: W,
}

pub enum State {
    Next,
    Initial,
}

pub struct Wrap<S>
where
    S: Sized,
{
    inner: S,
    state: State,
}

use std::{
    borrow::Cow,
    cell::{Cell, RefCell},
};

fn main() {
    // dealing with ref
    // dealing with ref mut twice

    let data = "Hello world";

    let mut r = &mut data;

    println!("Results:")
}
