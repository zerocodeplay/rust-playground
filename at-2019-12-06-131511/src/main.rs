// snippet of code @ 2019-12-06 13:15:11

// === Rust Playground ===
// This snippet is in: ~/.emacs.d/rust-playground/at-2019-12-06-131511/

// Execute the snippet: C-c C-c
// Delete the snippet completely: C-c k
// Toggle between main.rs and Cargo.toml: C-c b
use std::{
    collections::{hash_map::DefaultHasher, HashSet},
    hash::{Hash, Hasher},
};

pub(crate) type Index = (usize, usize);

enum State {
    HasNext { inner: HashSet<u64>, next_id: Index },
    Done,
}

pub struct A {
    inner: State,
}

impl A {
    fn next(&mut self) -> Option<(Index)> {}
}

#[test]
fn test_hash_vec_order() {
    let (mut h1, mut h2) = (DefaultHasher::new(), DefaultHasher::new());

    let data = vec![vec![1, 2, 3], vec![3, 2, 1], vec![2, 3, 1]];

    Hash::hash(&data[0], &mut h1);
    Hash::hash(&data[1], &mut h2);

    assert_eq!(h1.finish(), h2.finish());
}
