// snippet of code @ 2019-12-15 19:51:01

// === Rust Playground ===
// This snippet is in: ~/.emacs.d/rust-playground/at-2019-12-15-195101/

// Execute the snippet: C-c C-c
// Delete the snippet completely: C-c k
// Toggle between main.rs and Cargo.toml: C-c b

#[derive(Debug)]
pub struct DiskCache {
    cache_fd: u64,
    metadata_fd: u64,
    data_fd: u64,
}

#[derive(Debug)]
pub struct CacheIdx {
    hash: usize,
    sector_idx: usize,
    block_size: usize,
}

#[derive(Debug)]
pub struct CacheBlock<'a> {
    idx: usize,
    checksum: usize,
    data: &'a [u8],
}

trait BlockStore {
    const BLOCK_SIZE: usize;

    type Id;
    type Error;
    type Block;

    fn fetch_block(&self, key: Self::Id) -> Result<Item = Self::Block, Error = Self::Error>;

    fn update_block(
        &self,
        key: Self::Id,
        block: Self::Block,
    ) -> Result<Item = Self::Id, Error = Self::Error>;
}

#[derive(Debug)]
pub struct BlobRefs<Id, Sector>
where
    Id: Debug + Size,
    Sector: Debug + Size,
{
    indices: Vec<(Id, Sector)>
    fd: usize,
}

trait BlobStore {
    type Id;
    type Sector;
    type Blob;
    type Error;

    fn fetch_blob(
        &self,
        key: Self::Sector,
        size: usize,
    ) -> Result<Item = Self::Blob, Error = Self::Error>;

    fn append_blob<'a>(&self, data: [u8]) -> Result<Item = Self::Id, Error = Self::Error>;

    fn compress<'a>(fd: usize)
        -> Result<Item = BlobRefs>, Error = Self::Error>;
}

trait Storage {
    /// identifier
    type Id;
    /// key
    type Key;
    /// value
    type Value;

    fn fetch(&self, key: Self::Key) -> Result<Item = Self::Value, Error = Self::Error>;
    fn store(&mut self, key: Self::Value) -> Result<Self::Id, Error = Self::Error>;
    fn size(&self) -> usize;
}

trait Cache: Storage {
    fn capacity(&self) -> Option<usize>;
}

trait FromFile: Size + Drop {
    fn open<P>(path: P) -> Self where P: AsRef<P>;
    fn read_only<P>(path: P) -> Self where P: AsRef<P>;
}

fn main() {
    println!("Results:")
}
