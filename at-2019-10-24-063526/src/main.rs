// snippet of code @ 2019-10-24 06:35:28

// === Rust Playground ===
// This snippet is in: ~/.emacs.d/rust-playground/at-2019-10-24-063526/

// Execute the snippet: C-c C-c
// Delete the snippet completely: C-c k
// Toggle between main.rs and Cargo.toml: C-c b
extern crate fuse;
use fuse::{
    FileAttr, FileSystem, FileType, ReplyAttr, ReplyData, ReplyDirectory, ReplyEntry, Request,
};
use std::ffi::OsStr;

#[derive(Debug, Serialize, Deserialize)]
pub enum Policy {
    Allow, Disallow
}

#[derive(Debug, PartialEq, Hash)]
pub struct UserAction {
    context: Request,
    action:
}

pub trait AccessControl {
    fn check(&self, action: UserAction) -> Option<Policy>;
}

pub struct SimpleAcessControl<P> where P: Sized + Hash + PartialEq {
    inner: HashMap<P, Policy>
}

impl <P> AccessControl for AccessControl<P> {
    fn check(&self, action: UserAction) -> Option<Policy> {

    }
}

/// Overlay FS
///
/// It gets signature of the program & user that do syscall to filesystem.
/// check their signature whether it matches with allowed signature in filesystem or not
/// if it's matches, then continue.
///
pub struct OverlayFS {
    inner: AccessControl<UserAction>,

}

impl FileSystem for OverlayFS {
    fn init(&mut self, r: &Request) -> Result<(), c_int> {}
    fn destroy(&mut self, r: &Request) -> {}
    fn lookup(&mut self, r: &Request, parent: u64, name: &OsStr, reply: ReplyEntry) {}
    fn getattr(&mut self, r: &Request, ino: u64, reply: ReplyAttr) {}
    fn read(&mut self, r: &Request, ino: u64, fh: u64, offset: i64, size: u32, reply: ReplyData) {}
    fn readdir(&mut self, r: &Request, ino: u64, fh: u64, offset: i64, mut reply: ReplyDirectory) {}
    fn mkdir(&mut self, r: &Request, parent: u64, name: &OsStr, mode: u32, reply: ReplyEntry) {}
    fn open(&mut self, r: &Request, ino: u64, flags: u32, reply: ReplyOpen) {}

}

fn main() {
    println!("Results:")
}
