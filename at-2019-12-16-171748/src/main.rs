// snippet of code @ 2019-12-16 17:17:48

// === Rust Playground ===
// This snippet is in: ~/.emacs.d/rust-playground/at-2019-12-16-171748/

// Execute the snippet: C-c C-c
// Delete the snippet completely: C-c k
// Toggle between main.rs and Cargo.toml: C-c b

use std::{
    borrow::Cow,
    cell::Cell,
    cmp::Ordering,
    collections::{BinaryHeap, HashMap, HashSet},
    io::{BufRead, Cursor},
    iter::FromIterator,
};

#[derive(Debug, Clone)]
pub struct EnvPair {
    fields: Vec<String>,
    value: String,
}

/// This abstraction give us contract that
/// if the parent not exists, we could create only the branch
/// This save us to do combinatoric search for a parent sets
/// in the segment
///
/// the parent shouldn't be empty even it's first node.
///
/// [0, 1, 2] -> Segment { parent : [0, 1], branch: [2] }
///
/// we could savely said that if hole is exists, then create a new node based on it.
/// when traversing Segment will gradually move hole to parent.
///
/// example:
///
/// [[0, 1, 2], [0, 1, 3], [0, 1, 4, 5], [0, 1, 4, 6], [0, 1, 7, 8], [0, 1, 7, 9], [0, 1, 7, 10], [0, 11, 12, 13]]
///
/// hole: [0, 1], parent: [], leaf: 2
/// hole: [], parent: [0, 1], leaf: 3
/// hole: [4], parent: [0, 1], leaf: 5
/// hole: [], parent: [0, 1, 4], leaf: 6
/// hole: [7], parent: [0, 1], leaf: 8
/// hole: [], parent: [0, 1, 7], leaf: 9
/// hole: [], parent: [0, 1, 7], leaf: 10
/// hole: [11, 12], parent: [0], leaf: 13
///
#[derive(Debug, Clone)]
pub struct Segment {
    hole: Vec<usize>,
    parent: Vec<usize>,
    leaf: usize,
}

impl Segment {
    #[inline]
    pub fn parent(&self) -> &[usize] {
        self.parent.as_ref()
    }

    #[inline]
    pub fn hole(&self) -> &[usize] {
        self.hole.as_ref()
    }

    #[inline]
    pub fn leaf(&self) -> usize {
        self.leaf
    }

    #[inline]
    pub fn has_next(&self) -> bool {
        !self.hole.is_empty()
    }

    ///
    /// Next segment based on current segment.
    /// Sadly, this way, we allocated a lot of parent clones.
    ///
    /// We might be able to save some allocation by creating iterator ?
    ///
    /// Better to move this logic into the cursor of the envs
    ///
    #[inline]
    pub fn next(&self) -> Option<Segment> {
        if !self.hole.is_empty() {
            let mut parent = self.parent.clone();
            parent.push(self.hole[self.hole.len() - 1]);

            Some(Self {
                hole: self.hole[0..self.hole.len() - 2].into(),
                parent: parent,
                leaf: self.leaf,
            })
        } else {
            None
        }
    }
}

pub struct Envs<'a> {
    reverse: Vec<&'a str>,
    indices: Vec<Segment>,
    data: Vec<&'a str>,
}

impl<'a> Default for Envs<'a> {
    #[inline]
    fn default() -> Self {
        Self {
            reverse: Vec::with_capacity(0),
            indices: Vec::with_capacity(0),
            data: Vec::with_capacity(0),
        }
    }
}

impl<'a> Envs<'a> {
    #[inline]
    pub fn branch_of(&self, segment: &Segment) -> Option<Vec<&'a str>> {
        let mut branch = Vec::with_capacity(segment.hole.len() + segment.parent.len() + 1);

        for el in segment.parent() {
            match self.reverse.get(*el) {
                Some(p) => branch.push(*p),
                _ => return None,
            }
        }

        for el in segment.hole() {
            match self.reverse.get(*el) {
                Some(p) => branch.push(*p),
                _ => return None,
            }
        }

        Some(branch)
    }

    #[inline]
    pub fn hole_of(&self, segment: &Segment) -> Option<Vec<&'a str>> {
        let mut branch = Vec::with_capacity(segment.hole.len());

        for el in segment.hole() {
            match self.reverse.get(*el) {
                Some(p) => branch.push(*p),
                _ => return None,
            }
        }

        Some(branch)
    }

    #[inline]
    pub fn parent_of(&self, segment: &Segment) -> Option<Vec<&'a str>> {
        let mut branch = Vec::with_capacity(segment.parent.len());

        for el in segment.parent() {
            match self.reverse.get(*el) {
                Some(p) => branch.push(*p),
                _ => return None,
            }
        }

        Some(branch)
    }

    #[inline]
    pub fn from_str<'c>(
        raw: &'c str,
        comment: char,
        kv_sep: char,
        key_sep: &str,
    ) -> Result<Self, ()> {
        Self::from_reader(Cursor::new(raw), comment, kv_sep, key_sep)
    }

    pub fn from_reader<'c, R>(
        reader: R,
        comment: char,
        kv_sep: char,
        key_sep: &str,
    ) -> Result<Self, ()>
    where
        R: BufRead,
    {
        let data: BinaryHeap<EnvPair> = BinaryHeap::from_iter(reader.lines().filter_map(|r| {
            r.ok()
                .as_ref()
                .and_then(move |line| EnvPair::from_str(line, comment, kv_sep, key_sep))
        }));

        let mut inner = Self::default();
        let mut reverse_idx = HashMap::<String, usize>::new();
        //
        // env pair fields will always direct to a leaf node in the branch/tree
        // so, the only possible parent node would be fields[0-k-1] or
        // any subsequences fields that exists in the parent sets
        //
        // logic:
        // sets : {}
        // fields : [0 ... ii]
        //
        // sets : {[0 ... ii-1]}
        // fields : [0 ... ii-1 ... k]
        //
        // sets : {[0 ... ii-1], [ii-1 ... k-1], [0 ... k-1]}
        //
        // example:
        // [[0, 1, 2], [0, 1, 3], [0, 1, 4, 5], [0, 1, 4, 6], [0, 1, 7, 8], [0, 1, 7, 9], [0, 1, 7, 10], [0, 11, 12, 13]]
        //
        //
        //
        let mut parents = HashSet::<Vec<usize>>::new();

        let mut idx = 0;
        for EnvPair { fields, value } in data.iter() {
            let mut indices: Vec<usize> = Vec::new();

            for field in fields {
                let ridx = match reverse_idx.get(field) {
                    Some(ridx) => *ridx,
                    _ => {
                        let old_idx = idx;
                        reverse_idx.insert(field.to_string(), idx);
                        inner.reverse.push(field);
                        idx += 1;
                        old_idx
                    }
                };

                indices.push(ridx);

                if !parents.contains(indices.as_slice()) {
                    parents.insert(indices.clone());
                }
            }

            // do combinatoric checks for each leftmost subsequences &
        }

        Err(())
    }
}

impl PartialEq for EnvPair {
    #[inline]
    fn eq(&self, other: &EnvPair) -> bool {
        self.fields.eq(&other.fields)
    }
}

impl Eq for EnvPair {}

impl PartialOrd for EnvPair {
    fn partial_cmp(&self, other: &EnvPair) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl Ord for EnvPair {
    fn cmp(&self, other: &EnvPair) -> Ordering {
        self.fields.cmp(&other.fields)
    }
}

impl EnvPair {
    #[inline]
    pub fn from_str<'a>(line: &'a str, comment: char, kv_sep: char, key_sep: &str) -> Option<Self> {
        let line = line.trim();

        if line.starts_with(comment) {
            return None;
        }

        let pair = line
            .splitn(2, kv_sep)
            .map(move |line| line.to_string())
            .collect::<Vec<_>>();

        match &pair[..] {
            [key, value] => {
                let fields = key
                    .split(key_sep)
                    .map(move |line| line.to_string())
                    .collect::<Vec<_>>();

                Some(Self {
                    fields: fields,
                    value: value.clone(),
                })
            }
            _ => None,
        }
    }
}

fn main() {
    let raw = r#"CONFIG__DATABASE__NAME=name
CONFIG__DATABASE__CREDENTIAL__TYPE=password
CONFIG__DATABASE__CREDENTIAL__PASSWORD=some_password
CONFIG__DATABASE__CONNECTION__POOL=10
CONFIG__DATABASE__USERNAME=username
CONFIG__DATABASE__CONNECTION__TIMEOUT=10
CONFIG__DATABASE__CONNECTION__RETRIES=10,20,30
# CONFIG__APPLICATION__ENV=development
CONFIG__APPLICATION__LOGGER__LEVEL=info"#;
    let cursor = Cursor::new(raw);

    let envs = Envs::from_reader(cursor, '#', '=', "__");
}
