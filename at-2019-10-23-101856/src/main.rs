// snippet of code @ 2019-10-23 10:18:58

// === Rust Playground ===
// This snippet is in: ~/.emacs.d/rust-playground/at-2019-10-23-101856/

// Execute the snippet: C-c C-c
// Delete the snippet completely: C-c k
// Toggle between main.rs and Cargo.toml: C-c b

use tokio::{io, timer};
// try using parking_lot for Mutex
use std::{
    collections::HashMap,
    sync::{atomic::AtomicUsize, Arc, Mutex},
};

pub struct Runtime<T>
where
    T: Sized + Drop,
{
    counter: AtomicUsize,
    heap: HashMap<usize, T>,
}

pub type SharedRuntime = Mutex<Arc<Runtime>>;

fn main() {
    println!("Results:")
}
