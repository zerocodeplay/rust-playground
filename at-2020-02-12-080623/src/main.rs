// snippet of code @ 2020-02-12 08:06:23

// === Rust Playground ===
// This snippet is in: ~/.emacs.d/rust-playground/at-2020-02-12-080623/

// Execute the snippet: C-c C-c
// Delete the snippet completely: C-c k
// Toggle between main.rs and Cargo.toml: C-c b

use flate2::{write::ZlibEncoder, Compression};
use ring::aead::{Aad, Algorithm, NonceSequence, SealingKwy};

fn main() {
    println!("Results:")
}
