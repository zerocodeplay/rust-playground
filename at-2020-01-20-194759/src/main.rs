// snippet of code @ 2020-01-20 19:47:59

// === Rust Playground ===
// This snippet is in: ~/.emacs.d/rust-playground/at-2020-01-20-194759/

// Execute the snippet: C-c C-c
// Delete the snippet completely: C-c k
// Toggle between main.rs and Cargo.toml: C-c b

use std::{
    future::Future,
    pin::Pin,
    task::{Context, Poll},
};

/// important trait :
/// DerefMut, Deref, Unpin
///
/// if your type is not implement Unpin, you can't safely
/// call Pin::get_mut_unchecked(_).
///
///
/// Pin projection
/// - F ~ Unpin
///
/// this only needed if you need modify pinned Self
/// (or get &mut Self from Pinned mut self)
/// but most of Future struct will modify itself
/// unless for future::ready(_)
///
/// impl <F: Future + Unpin> Unpin for FutureC<F> {}
///
/// - Drop implementation shouldn't move things around
///
/// Pin::as_mut might only be available safely if
/// the T is either of Box, Rc, or any container types
/// but rarely can't be used for &'_ T.
///
/// Since Pin will reflect reference type (mut or non mut)
/// of type T if T isn't being allocated in stack, since
/// new rule has been added :
///
/// impl <'a, T> !DerefMut for &'a T {}
///
/// while DerefMut for Pin is only available if T implement DerefMut too
///
/// impl <T> DerefMut for Pin<T> where T: DerefMut, <T as Deref>::Target : Unpin {}
///
/// For any child struct dependent to pinned struct, it's always better to
/// have Pin projection method so that user doesn't need to do a lot of boilerplate.

pub struct Sample {
    value: usize,
}

fn main() {

    let pinned = Pin::new_unchecked
}
