// snippet of code @ 2020-02-07 20:34:31

// === Rust Playground ===
// This snippet is in: ~/.emacs.d/rust-playground/at-2020-02-07-203431/

// Execute the snippet: C-c C-c
// Delete the snippet completely: C-c k
// Toggle between main.rs and Cargo.toml: C-c b

use std::future::Future;

pub struct A {
    output: usize
};

impl Future for A {
    fn poll(&self : Pin<&mut Self>, ctx: &mut Context) -> Poll::<Self::Output> {
        Poll::Ready(Option::Some(1))
    }
}

fn main() {

    println!("Results:")
}
