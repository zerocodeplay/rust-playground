// snippet of code @ 2019-10-19 15:53:56

// === Rust Playground ===
// This snippet is in: ~/.emacs.d/rust-playground/at-2019-10-19-155353/

// Execute the snippet: C-c C-c
// Delete the snippet completely: C-c k
// Toggle between main.rs and Cargo.toml: C-c b
#![feature(slice_patterns)]
use std::{borrow::Cow, collections::HashMap};

#[derive(Debug, Clone, PartialEq)]
pub enum Number {
    Integer(i64),
    Double(f64),
}

type Optional<T> = Option<Box<T>>;

#[derive(Debug, Clone, PartialEq)]
pub enum Literal {
    Number(Number),
    String(String),
    Bool(bool),
    Optional(Optional<Literal>),
}

#[derive(Debug, Clone, PartialEq)]
pub enum Value {
    Literal(Literal),
    Dictionary(HashMap<String, Value>),
    Array(Vec<Value>),
}

pub trait Queryable {
    const ARRAY_BLOCK: [char; 2];
    const DICT_SEP: char;

    type Output;
    type Error;

    fn query(&self, path: &str) -> Result<Self::Output, Self::Error>;
}

fn index_parse(key: &str, block: &[char; 2]) -> Result<usize, ()> {
    if key.starts_with(block[0]) && key.ends_with(block[1]) && key.len() > 2 {
        key[1..key.len() - 1].parse::<usize>().map_err(|_| ())
    } else {
        Err(())
    }
}

impl Queryable for Value {
    const ARRAY_BLOCK: [char; 2] = ['[', ']'];
    const DICT_SEP: char = '.';

    type Output = Self;
    type Error = ();

    fn query(&self, path: &str) -> Result<Self::Output, Self::Error> {
        let tokens = path.splitn(1, Self::DICT_SEP).collect::<Vec<_>>();
        let slices = tokens.as_slice();

        match self {
            Value::Dictionary(d) => match slices {
                &[key, next] => {
                    let v = d.get(key);

                    match v {
                        Some(vv) => vv.query(next),
                        None => Err(()),
                    }
                }
                &[key] => d.get(key).map(|v| v.clone()).ok_or(()),
                // when querying empty string this should be expected
                // return None rather than self
                _ => Err(()),
            },
            Value::Array(a) => match slices {
                &[key, next] => {
                    let index = index_parse(key, &Self::ARRAY_BLOCK)?;
                    match a.get(index) {
                        Some(child) => child.query(next),
                        None => Err(()),
                    }
                }
                &[key] => {
                    let index = index_parse(key, &Self::ARRAY_BLOCK)?;
                    a.get(index).map(|v| v.clone()).ok_or(())
                }
                _ => Err(()),
            },
            Value::Literal(_) => Err(()),
        }
    }
}

pub fn lookup<'a, S, V>(v: V, query: S) -> Result<V::Output, V::Error>
where
    S: Into<Cow<'a, str>>,
    V: Queryable,
{
    v.query(&query.into())
}

fn main() {
    let value: Value = Value::Array(vec![Value::Literal(Literal::String("Hello world".into()))]);

    let result = lookup(value, "[11]");

    println!("result: value[0] = {:?}", result);
}
