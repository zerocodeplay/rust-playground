#![feature(pattern)]
// snippet of code @ 2019-11-29 13:26:08

// === Rust Playground ===
// This snippet is in: ~/.emacs.d/rust-playground/at-2019-11-29-132608/

// Execute the snippet: C-c C-c
// Delete the snippet completely: C-c k
// Toggle between main.rs and Cargo.toml: C-c b

static RAW_DATA: &'static str = r#"CONFIG__DATABASE__NAME=name
  CONFIG__DATABASE__USERNAME=username
  CONFIG__DATABASE__URL=mysql://host:port
  CONFIG__DATABASE__CREDENTIAL__TYPE=password
  CONFIG__DATABASE__CREDENTIAL__PASSWORD=some_password
  CONFIG__DATABASE__CONNECTION__POOL=10
  CONFIG__DATABASE__CONNECTION__TIMEOUT=10
  CONFIG__DATABASE__CONNECTION__RETRY=10,20,30
  CONFIG__APPLICATION__ENV=development
  CONFIG__APPLICATION__LOGGER__LEVEL=info"#;

use derive_builder::Builder;
use std::{
    io,
    borrow::Cow,
    collections::{binary_heap, BinaryHeap, HashSet},
    iter::FromIterator,
    str::pattern::Pattern,
};

pub struct StringDict<'a> {
    data: Vec<&'a str>,
    indices: HashSet<Vec<u8>>,
}

/// Node (StringDict) (Key, Value)

pub type Key<'b> = Vec<Cow<'b, str>>;
pub type Index = Vec<usize>;

// impl<'a> StringDict<'a> {
//     #[inline]
//     pub fn from_str<'b>(iter: binary_heap::Iter<Cow<'b, str>>) -> Result<Self, ()> {
//         let mut data = vec![];
//         let mut indices = HashSet::new();

//         Err(())
//     }

//     pub fn construct<'b>(&self, index: Index) -> Result<Key<'b>, ()>
//     where
//         'b: 'a,
//     {
//         index
//             .iter()
//             .map(move |idx| Cow::from(self.data.get(idx)))
//             .collect::<Vec<_>>()
//     }
// }


#[derive(Builder)]
pub struct Options {
    pair_sep: Option<char>,
}

pub fn from_reader<'a, R>(reader: R, options: ) -> Result<(), ()> where R: io::BufRead {
    reader.lines().filter_map(move |line| line.and_then(move |line| {
        let line = line.trim();

        if !line.starts_with('#') {
            let pair = line.split('=').map(move |v| v.trim()).collect::<Vec<&str>>();

            match &pair[..] {
                [key, value] => {

                },
                _ None
            }
        }
    }));

    Ok(())
}

fn main() {
    println!("raw_data: {}", RAW_DATA);
}
