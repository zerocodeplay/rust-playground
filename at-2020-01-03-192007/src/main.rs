// snippet of code @ 2020-01-03 19:20:07

// === Rust Playground ===
// This snippet is in: ~/.emacs.d/rust-playground/at-2020-01-03-192007/

// Execute the snippet: C-c C-c
// Delete the snippet completely: C-c k
// Toggle between main.rs and Cargo.toml: C-c b

#![feature(const_generics, associated_type_defaults)]
use std::{collections::BinaryHeap, fmt};

impl<const A: usize> fmt::Debug for [char; A] {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {}
}

// plate number [Area ID, Number, Kind]
pub struct Plate<const A: usize, const K: usize> {
    area_id: [char; A],
    number: u16,
    kind: [char; K],
}

impl<const A: usize, const K: usize> fmt::Debug for Plate<A, K> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{:?}-{}-{:?}", self.area_id, self.number, self.kind)
    }
}

#[derive(PartialEq, Eq, Debug)]
pub struct Color([u8; 3]);

pub mod error {
    use std::{error::Error as StdError, fmt};

    #[derive(Debug)]
    pub enum Error<S>
    where
        S: fmt::Debug,
    {
        // when parking lot try to park given slot
        // but the slot is out of range
        SlotNotFound(S),
        // when parking lot try to reserve
        // a car but no such slot available yet
        ParkFullError,
    }
}

pub mod types {
    use super::{error::Error, Color, Plate};
    use std::{fmt, ops::Index};

    //
    // Vehicle has a plate number and has a color.
    //
    pub trait Vehicle<const A: usize, const K: usize> {
        fn plate(&self) -> &'_ Plate<A, K>;
        fn color(&self) -> Color;
    }

    pub trait ParkingLot<'a> {
        // error that represents parking lot status
        // after mutating action performed
        type Error = Error<Self::Slot>;
        //
        //
        type Status: Index<Self::Slot, Output = Self::Vehicle>;
        // slot is container that contains both
        // SlotRef and VehicleRef
        type Slot: fmt::Debug;
        type SlotRef;
        // Vehicle is an actual Vehicle instance
        type Vehicle;
        // VehicleRef is a primary key that identify
        // a vehicle (distinct)
        // ex: Plate number. see [`Plate`].
        type VehicleRef;

        // park the cars into nearest (from the entry) available slots
        //
        fn park(&mut self, vehicle: Self::Vehicle) -> Result<Self::Slot, Self::Error>;

        // mark the slot to be available and remove the car from container.
        //
        fn leave(&mut self, slot: Self::SlotRef) -> Result<Self::Slot, Self::Error>;

        fn status(&self) -> &'a Self::Status;
        fn slots(&self, color: &Color) -> Iterator<Item = &'a Self::Slot>;
        fn vehicles(&self, color: &Color) -> Iterator<Item = &'a Self::Vehicle>;

        fn slot_of_ref(&self, r: Self::VehicleRef) -> Option<Self::Slot>;
        fn slot_of(&self, vehicle: Self::Vehicle) -> Option<Self::Slot>;
        fn size(&self) -> usize;
    }
}

use types::Vehicle;

/// `Car` vehicle type kinds.
///
#[derive(Debug)]
pub struct Car {
    plate: Plate<2, 2>,
    color: Color,
}

impl Vehicle<2, 2> for Car {
    #[inline]
    fn plate(&self) -> &'_ Plate<2, 2> {
        &self.plate
    }

    #[inline]
    fn color(&self) -> Color {
        self.color
    }
}

/// newtype of usize that represents index in slot.
pub type SlotRef = usize;

///
/// Slot container of SlotRef.
///
pub struct Slot<'a, V>(SlotRef, &'a mut Option<&'a V>)
where
    V: Sized;

pub struct FixedParkingLot {
    // maintain attained free slots
    // as queue
    attained_slots: BinaryHeap<SlotRef>,
}

// impl<'a> IntoIter<'a> for &'a InMemoryParkingLot {
//    type Item = (Self::Status::Idx, Self::Status::Output);
// }

// impl<'a> IntoIter<'a> for ParkingLot<'a> {
//     type Item = (Self::Status::Idx, Self::Status::Ouput);
// }

// parking lot
// - car always parks in the free slots near entry
// - when a car leaves, the slot being attained
// - we could query the cars & slots given by color,
// - we could fetch (slots) where the cars being parked

fn main() {}
