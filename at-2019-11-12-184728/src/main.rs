// snippet of code @ 2019-11-12 18:47:34

// === Rust Playground ===
// This snippet is in: ~/.emacs.d/rust-playground/at-2019-11-12-184728/

// Execute the snippet: C-c C-c
// Delete the snippet completely: C-c k
// Toggle between main.rs and Cargo.toml: C-c b

extern crate serde;

use serde::de;
use std::{
    borrow::Cow,
    collections::{binary_heap, BinaryHeap},
    error::Error as StdError,
    fmt,
    io::{self, Read},
    iter::FromIterator,
};

#[derive(Debug)]
struct FieldMap<'de> {
    de: &'de mut Deserializer,
}

///
/// https://github.com/nox/serde_urlencoded
/// - we almost have the same kind of problem this, with the difference
///   when iterating the key the MapAccess need to know which parent is it
///
/// https://github.com/serde-rs/serde/issues/1041
/// https://docs.rs/serde/1.0.102/src/serde/de/mod.rs.html#1766
///
/// Apparently using MapAccess, you need to divide how to access key & value iteratively.
///
///
impl<'de> de::MapAccess<'de> for FieldMap<'de> {
    type Error = Error;

    fn next_key_seed<K>(&mut self, seed: K) -> Result<Option<K::Value>, Self::Error>
    where
        K: de::DeserializeSeed<'de>,
    {
        Err(Self::Error::Unsupported)
    }

    fn next_value_seed<V>(&mut self, seed: V) -> Result<V::Value, Self::Error>
    where
        V: de::DeserializeSeed<'de>,
    {
        Err(Self::Error::Unsupported)
    }
}


struct SequenceLiteral<'de> {
    de: &'de mut Deserializer,
    raw: String,
}

///
///
///
impl<'de> de::SeqAccess<'de> for SequenceLiteral<'de> {
    type Error = Error;

    fn next_element_seed<T>(&mut self, seed: T) -> Result<Option<T::Value>, Self::Error>
    where
        T: DeserializeSeed<'de>,
    {
        let raw = raw.trim();

        let raw.is_empty() {
            Err(Self::Error::UnknownError)
        } else if raw.starts_with('[') && raw.ends_with(']') {
            // detect array value
            // if its an array split based on ',' (careful of quoted str)
            let slices = Cow::from(raw[1..raw.len() - 1].trim());

            if slices.is_empty() {
                Ok(Self::default())
            } else {
                let mut data = vec![];
                let mut cursor = slices;
                //
                // quote escape by using lookup separator function.
                // the idea is actively looking for separator but also
                // do escape quotation by skipping quoted str.
                //
                // when idx where separator found, slice the str in [idx + 1]
                //
                // TODO: I might need to store this into impl DeserializeSeed so that
                // it can be consumed by Deserializer
                //
                loop {
                    let part_idx = lookup_array_sep(cursor.clone(), ',').unwrap_or(cursor.len());
                    let current = &cursor[0..part_idx];

                    // let el = Literal::parse(current).map_err(Self::Error::LiteralError)?;
                    // data.push(el);
                    // TODO: visitor should visit each element by calling ValueVisitor
                    //       or deserialize_any (again?)
                    // seed.deserialize(current)?;

                    // TODO @zerosign : there should be better way to do this
                    // this need to re assign cursor but cursor already being borrowed in `Literal::parse`
                    if part_idx < cursor.len() - 1 {
                        cursor = String::from(&cursor[part_idx + 1..cursor.len()]).into();
                    } else {
                        break;
                    }
                }

                Ok(Self(data))
            }

        }
    }
}


pub struct Deserializer {
    inner: binary_heap::IntoIter<(Vec<String>, String)>,
}

pub fn from_reader<R>(r: R) -> Result<Deserializer, io::Error>
where
    R: io::BufRead,
{
    Ok(Deserializer {
        inner: BinaryHeap::from_iter(
            r.lines() // Iterator<Item = Result<String, io::Error>>
                .filter_map(move |r| {
                    r.ok().and_then(move |line| {
                        let line = line.split('=').collect::<Vec<&str>>();
                        match line[..] {
                            [key, value] => Some((
                                key.split("__").map(String::from).collect::<Vec<String>>(),
                                String::from(value),
                            )),
                            _ => None,
                        }
                    })
                }),
        )
        .into_iter(),
        state: None,
    })
}

#[derive(Debug)]
pub enum Error {
    UnknownError,
    Unsupported,
    CustomError(String),
    MissingField(String),
    EndOfFile,
}

impl StdError for Error {}

impl de::Error for Error {
    fn custom<T: fmt::Display>(msg: T) -> Self {
        Error::CustomError(format!("{}", msg))
    }

    fn missing_field(field: &'static str) -> Self {
        Error::MissingField(String::from(field))
    }
}

impl fmt::Display for Error {
    fn fmt(&self, fmt: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::UnknownError => write!(fmt, "unknown error"),
            Self::CustomError(s) => write!(fmt, "custom error of {}", s),
            Self::MissingField(s) => write!(fmt, "missing field {}", s),
            Self::Unsupported => write!(fmt, "unsupported"),
            Self::EndOfFile => write!(fmt, "end of file reached"),
        }
    }
}

macro_rules! unsupported_defaults {
    ($($de:ident),*) => { $(
        fn $de<V>(self, visitor: V) -> Result<V::Value, Self::Error>
        where
            V: de::Visitor<'de> {
                Err(Self::Error::Unsupported)
            }

    )*};
}

macro_rules! forward_calls {
    ([($fn:ident),*] => $target:ident) => $({
        fn $de<V>(self, visitor: V) -> Result<V::Value, Self::Error>
        where
            V: de::Visitor<'de> {
            visitor.$target(self.)
        }
    })
}



impl<'a, 'de> de::Deserializer<'de> for &'a mut Deserializer
where
    'a: 'de,
{
    type Error = Error;

    fn deserialize_any<V>(self, visitor: V) -> Result<V::Value, Self::Error>
    where
        V: de::Visitor<'de>,
    {
        self.deserialize_map(visitor)
    }

    fn deserialize_bool<V>(self, visitor: V) -> Result<V::Value, Self::Error>
    where
        V: de::Visitor<'de>,
    {
        // TODO: need cursor in here
        visitor.visit_bool(bool::parse(raw).map(Self::Error::BoolError)?)
    }

    fn deserialize_string<V>(self, visitor: V) -> Result<V::Value, Self::Error>
    where
        V: de::Visitor<'de>,
    {
        // TODO: need cursor in here
        visitor.visit_string(raw)
    }

    unsupported_defaults! {
        deserialize_i8, deserialize_i16, deserialize_i32,
        deserialize_i64, deserialize_u8, deserialize_u16, deserialize_u32,
        deserialize_u64, deserialize_f32, deserialize_f64, deserialize_char,
        deserialize_str, deserialize_string, deserialize_bytes, deserialize_byte_buf,
        deserialize_option, deserialize_unit,
        deserialize_seq, deserialize_identifier, deserialize_ignored_any
    }

    fn deserialize_unit_struct<V>(self, field: &'static str, v: V) -> Result<V::Value, Self::Error>
    where
        V: de::Visitor<'de>,
    {


        Err(Self::Error::Unsupported)
    }

    fn deserialize_newtype_struct<V>(
        self,
        field: &'static str,
        v: V,
    ) -> Result<V::Value, Self::Error>
    where
        V: de::Visitor<'de>,
    {
        Err(Self::Error::UnknownError)
    }

    fn deserialize_tuple<V>(self, idx: usize, v: V) -> Result<V::Value, Self::Error>
    where
        V: de::Visitor<'de>,
    {
        Err(Self::Error::Unsupported)
    }

    fn deserialize_map<V>(self, v: V) -> Result<V::Value, Self::Error>
    where
        V: de::Visitor<'de>,
    {
        v.visit_map(FieldMap { de: self })
    }

    fn deserialize_tuple_struct<V>(
        self,
        field: &'static str,
        idx: usize,
        v: V,
    ) -> Result<V::Value, Self::Error>
    where
        V: de::Visitor<'de>,
    {
        Err(Self::Error::Unsupported)
    }

    fn deserialize_struct<V>(
        self,
        field: &'static str,
        fields: &'static [&'static str],
        v: V,
    ) -> Result<V::Value, Self::Error>
    where
        V: de::Visitor<'de>,
    {
        Err(Self::Error::UnknownError)
    }

    fn deserialize_enum<V>(
        self,
        field: &'static str,
        fields: &'static [&'static str],
        v: V,
    ) -> Result<V::Value, Self::Error>
    where
        V: de::Visitor<'de>,
    {
        Err(Self::Error::UnknownError)
    }
}

fn main() {


}
