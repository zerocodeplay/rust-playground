// snippet of code @ 2019-12-17 22:32:24

// === Rust Playground ===
// This snippet is in: ~/.emacs.d/rust-playground/at-2019-12-17-223224/

// Execute the snippet: C-c C-c
// Delete the snippet completely: C-c k
// Toggle between main.rs and Cargo.toml: C-c b

use std::{
    fs::{File, OpenOptions},
    io::{BufRead, Cursor, Read},
    process,
};

fn main() {
    let mut buffer = String::new();
    let mut file = OpenOptions::new()
        .read(true)
        .write(false)
        .create(false)
        .open(format!("/proc/{}/cmdline", process::id()))
        .expect("can't open file");

    file.read_to_string(&mut buffer).expect("can't read file");

    let args = buffer.split('\0').collect::<Vec<_>>();

    println!("args: {:?}", args);
}
