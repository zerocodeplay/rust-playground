// snippet of code @ 2019-12-01 19:13:09

// === Rust Playground ===
// This snippet is in: ~/.emacs.d/rust-playground/at-2019-12-01-191309/

// Execute the snippet: C-c C-c
// Delete the snippet completely: C-c k
// Toggle between main.rs and Cargo.toml: C-c b
use semver::Version;
use std::borrow::Cow;
use std::collections::HashMap;

#[derive(Debug)]
pub enum Kind {
    String,
    Bool,
    Int,
    Double,
    List(Box<Kind>),
    Option(Box<Kind>),
}

#[derive(Debug)]
pub enum Value {
    String(String),
    Bool(bool),
    Int(i64),
    Double(f64),
    List(Vec<Value>),
}

#[derive(Debug)]
pub struct OptionInfo {
    name: String,
    alias: char,
    kind: Kind,
    description: Option<String>,
    default: Option<Value>,
}

#[derive(Debug)]
pub struct ActionInfo {
    action: String,
    version: Option<Version>,
    description: Option<String>,
    options: HashMap<String, OptionInfo>,
}

#[derive(Debug)]
pub struct GroupInfo {
    name: String,
    version: Option<Version>,
    actions: HashMap<String, ActionInfo>,
    subgroup: HashMap<String, GroupInfo>,
    description: Option<String>,
}

fn main() {}
