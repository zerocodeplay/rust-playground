// snippet of code @ 2020-02-07 22:36:20

// === Rust Playground ===
// This snippet is in: ~/.emacs.d/rust-playground/at-2020-02-07-223620/

// Execute the snippet: C-c C-c
// Delete the snippet completely: C-c k
// Toggle between main.rs and Cargo.toml: C-c b

use std::path::Path;

pub struct Project<P>
where
    P: AsRef<Path> + Sized,
{
    name: String,
    dir: (P, P),
    sql: P,
    uri: String,
}

pub struct Configuration {
    name: String,
    uri: String
}

pub enum Error {
    IoError(io::Error),
    ...
}

impl From<io::Error> for Error {
    fn from(other: io::Error) -> Self {
        Self::IoError(other)
    }
}

pub fn save<P, W>(config: &Configuration, path: P) -> Result<Error, ()> where P: AsRef<Path> + Sized, W: io::Write {
    fs::write(serde_json::to_string_pretty(&config)?;
}

impl Configuration {
    #[inline]
    pub fn from_file<P>(p: P) -> Result<Error, Self>
    where P: AsRef<Path> {
        let config = String::from(fs::read_to_string(p)?);
        serde_json::from_str(&config)?
    }

    #[inline]
    pub fn default() -> Result<Error, Self> {
        dirs::home_dir().map(str::join(".cid"))
    }
}

fn main() {
    println!("Results:")
}
