// snippet of code @ 2020-01-20 15:37:01

// === Rust Playground ===
// This snippet is in: ~/.emacs.d/rust-playground/at-2020-01-20-153701/

// Execute the snippet: C-c C-c
// Delete the snippet completely: C-c k
// Toggle between main.rs and Cargo.toml: C-c b

use std::{
    future::Future,
    pin::Pin,
    task::{Context, Poll},
    time::{Duration, Instant},
};

use futures::{
    future::{self, FutureExt},
    stream::StreamExt,
    Stream,
};

use tokio::runtime::{Builder, Handle, Runtime};

#[derive(Debug)]
pub enum Output {
    Timeout,
    Error,
    Success(usize),
}

#[derive(Debug, Default)]
pub struct Metric {
    // this shiuld be safe since
    // metric will only being accessible for 1 thread at a time.
    times: usize,
}

pub struct Times<'a, F>
where
    F: Future<Output = Output> + Unpin,
{
    inner: F,
    times: usize,
    duration: Option<Duration>,
    metric: Metric,
    // this to fetch whether the duration is done or not
    initial: Option<&'a mut Instant>,
}

impl<'a, F> Times<'a, F>
where
    F: Future<Output = Output> + Unpin,
{
    pub fn new(f: F, times: usize, duration: Option<Duration>) -> Self {
        Self {
            inner: f,
            times: 1,
            duration: duration,
            metric: Metric::default(),
            initial: None,
        }
    }

    #[inline]
    pub fn inner(self: Pin<&mut Self>) -> Pin<&mut F> {
        // this is required since
        // the only way to fetch self.inner is to do
        // field projection on Pin<&mut Self>
        //
        unsafe {
            let this: &mut Self = Pin::get_unchecked_mut(self);
            Pin::new_unchecked(&mut this.inner)
        }
    }

    #[inline]
    pub fn times(self: Pin<&mut Self>) -> Pin<&mut usize> {
        unsafe {
            let this: &mut Self = Pin::get_unchecked_mut(self);
            Pin::new_unchecked(&mut this.times)
        }
    }

    #[inline]
    pub fn duration(self: Pin<&mut Self>) -> Pin<&mut Option<Duration>> {
        unsafe {
            let this: &mut Self = Pin::get_unchecked_mut(self);
            Pin::new_unchecked(&mut this.duration)
        }
    }

    #[inline]
    pub fn metric(self: Pin<&mut Self>) -> Pin<&mut Metric> {
        unsafe {
            let this: &mut Self = Pin::get_unchecked_mut(self);
            Pin::new_unchecked(&mut this.metric)
        }
    }

    #[inline]
    pub fn initial(self: Pin<&mut Self>) -> Pin<&mut Option<&'a mut Instant>> {
        unsafe {
            let this: &mut Self = Pin::get_unchecked_mut(self);
            Pin::new_unchecked(&mut this.initial)
        }
    }
}

impl<'a, F> Stream for Times<'a, F>
where
    F: Future<Output = Output> + Unpin,
{
    type Item = Output;

    #[inline]
    fn poll_next(self: Pin<&mut Self>, context: &mut Context) -> Poll<Option<Self::Output>> {
        let mut times = self.times();
        let mut metric = self.metric();
        let current = times.get_ref();

        if current > 0 {
            match self.inner().poll() {
                Poll::Ready(out) => {
                    // decrement only if Poll::Ready
                    times.set(current);
                    Poll::Ready(Some(out))
                }
                Poll::Pending => Poll::Pending,
            }
        } else {
            Poll::Ready(None),
        }
    }
}

// impl<'a, F> Future for Applicative<'a, F>
// where
//     F: Future<Output = Output> + Unpin,
// {
//     type Output = Output;

//     #[inline]
//     fn poll(self: Pin<&mut Self>, context: &mut Context) -> Poll<Self::Output> {
//         self.inner().poll(context)
//     }
// }

#[inline]
pub fn example() -> impl Future<Output = Output> {
    future::ready(Output::Success(10))
}

fn main() {
    let mut runtime = Builder::new()
        .threaded_scheduler()
        .core_threads(8)
        .enable_io()
        .enable_time()
        .thread_name("default-pool")
        .build()
        .expect("can't build the runtime");

    let app = Times::new(example(), None, None);
    // let output = runtime.block_on(app);

    // println!("output: {:?}", output);
}
